const User = require("../models/user_model")

const createUser = async (req, res) => {

    try {
        const user = new User(req.body)
        const token = await user.generateAuthToken()
        await user.save()
        res.status(201).send({user, token})
    } catch (e) {
        res.status(400).send(e)
    }
}

const userProfile = async (req, res) => {
    res.send(req.user)
}

const updateUser = async (req, res) => {

    const updates = Object.keys(req.body)
    const allowedUpdates = ["name", "email", "password", "age"]
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        return res.status(400).send({ error: "Invalid updates!"})
    }

    try {
        updates.forEach((update) => req.user[update] = req.body[update])
        await req.user.save()
        res.send(req.user)
    } catch (e) {
        res.status(400).send(e)
    }
}

const deleteUser = async (req, res) => {
    try {
        await req.user.remove()
        res.send(req.user)

    } catch (e) {
        res.status(500).send()
    }
}

const userLogin = async (req, res) => {

    try {
        const user = await User.findByCredentials(req.body.email, req.body.password)
        const token = await user.generateAuthToken()
        console.log("user -------------> ", user)
        res.send({user, token})
    } catch (e) {
        res.status(400).send()
    }
}

const userLogout = async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.user.save()

        res.send()
    } catch (e) {
        res.status(500).send()
    }
}

const userLogoutAll = async (req, res) => {
    try {
        req.user.tokens = []
        await req.user.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
}

exports.createUser = createUser;
exports.userProfile = userProfile;
exports.updateUser = updateUser;
exports.deleteUser = deleteUser;
exports.userLogin = userLogin;
exports.userLogout = userLogout;
exports.userLogoutAll = userLogoutAll;