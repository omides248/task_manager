express = require("express")
const taskControllers = require("../controllers/task_controller")
const auth = require("../middleware/auth")

router = express.Router()

router.post("/tasks", auth, taskControllers.createTask)
router.get("/tasks/:id", auth, taskControllers.getTask)
router.patch("/tasks/:id", auth, taskControllers.updateTask)
router.delete("/tasks/:id", auth, taskControllers.deleteTask)

module.exports = router


