const express = require("express")
const auth = require("../middleware/auth")
const userControllers = require("../controllers/user_controller")

const router = new express.Router()

router.post("/users", userControllers.createUser)
router.get("/users/me", auth, userControllers.userProfile)
router.patch("/users/me",auth, userControllers.updateUser)
router.delete("/users/me", auth, userControllers.deleteUser)
router.post("/users/login", userControllers.userLogin)
router.post("/users/logout", auth, userControllers.userLogout)
router.post("/users/logout", auth, userControllers.userLogoutAll)

module.exports = router