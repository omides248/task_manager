const express = require("express")
const compression = require('compression')
require("./config/mongoose")
const userRouter = require("./routes/user_routes")
const taskRouter = require("./routes/task_routes")

const app = express()
const port = process.env.PORT || 3000

// app.use((req, res, next) => {
//     if (req.method === "GET") {
//         res.send("GET requests are disabled")
//     } else {
//         next()
//     }
// })

// app.use((req, res, next) => {
//     res.status(503).send("Site is currently down. Check back soon!")
// })

app.use(express.json()) // Mounts the specified middleware function or functions at the specified path
app.use(compression()) // Gzip compressing can greatly decrease the size of the response body and hence increase the speed of a web app.

// Register Router
app.use(userRouter) // Mounts the specified middleware function or functions at the specified path
app.use(taskRouter) // Mounts the specified middleware function or functions at the specified path


app.listen(port, () => {
    console.log("Server is up on port " + port)
})

const Task = require("./models/task_model")
const User = require("./models/user_model")

const main = async() => {

    // Real
    // const task = await Task.findById("5e69a5e2ba2afb266449265a")
    // await task.populate("owner").execPopulate()
    // console.log(task.owner)

    // virtual
    // const user = await User.findById("5e699ec5065e992944959730")
    // await user.populate("tasks").execPopulate()
    // console.log(user.tasks)

}

main()