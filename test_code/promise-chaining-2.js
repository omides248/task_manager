require("../config/mongoose")
const Task = require("../models/task_model")

// Task.findByIdAndDelete("5e64df976dfe3542f05b7535").then((task) => {
//     console.log(task)
//     return Task.countDocuments({completed: false})
// }).then((result) => {
//     console.log(result)
// }).catch((e) => {
//     console.log(e)
// })


const deleteTaskAndCount = async (id) => {
    const task = await Task.findByIdAndDelete(id)
    return await Task.countDocuments({completed: true})
}

deleteTaskAndCount("5e64df656dfe3542f05b7533").then((count) => {
    console.log(count)
}).catch((e) => {
    console.log(e)
})