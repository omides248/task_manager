require("../config/mongoose")
const User = require("../models/user_model")


// User.findByIdAndUpdate("5e64dbcf4fdb8c1d88934c6e", {age: 1}).then((user) => {
//     console.log(user)
//     return User.countDocuments({age: 1})
// }).then((result) => {
//     console.log(result)
// }).catch((e) => {
//     console.log(e)
// })


const updateAgeAndCount = async (id, age) => {
    const user = await User.findByIdAndUpdate(id, {age})
    return await User.countDocuments({age})
}

updateAgeAndCount("5e64dbcf4fdb8c1d88934c6e", 2).then((count) => {
    console.log(count)
}).catch((e) => {
    console.log(e)
})