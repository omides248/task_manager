students = [
    {
        "name": "ali",
        "age": 17
    },
    {
        "name": "omid",
        "age": 23
    },
    {
        "name": "hasan",
        "age": 24
    },
    {
        "name": "amin",
        "age": 15
    },
    {
        "name": "hamed",
        "age": 22
    }
]



// let old_students = []
//
// for (let i = 0; i < students.length; i++) {
//     if (students[i].age > 22) {
//         old_students.push(students[i])
//     }
// }

let old_students = students.filter((student) => student.age > 22)

console.log(old_students)

// tokens = [
//     {
//         "token": "1"
//     },
//     {
//         "token": "2"
//     },
//     {
//         "token": "3"
//     }
// ]
//
// logout_token = "3"
// new_tokens = tokens.filter((token) => {
//     // console.log(token)
//     console.log(token.token, " --- ", logout_token, ">>>>> ", token.token !== logout_token)
//     return token.token !== logout_token
// })
//
// console.log(new_tokens)